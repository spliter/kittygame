This is a simple simulation of evolution in grid-based cells.
Each cell can evolve by passing a modified genome to its children in the next generation, the cell will the execute its genome during its lifetime.
Each cell works like a simple CPU with memory.


The simulation already comes with the libraries needed, all you need is Visual C++ installed with windows SDK.



execute build.exe to build it and then run.exe to run it

alternatively you can download an already running version here:
http://www.catboxbeta.com/files/programs/EvolutionSimulation.zip