/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * vec4f.h
 *
 *  Created on: 18 sty 2015
 *      Author: Miko Kuta
 */

#ifndef VEC4F_H_
#define VEC4F_H_

#include "../shared/types.h"

struct vec4f
{

	union
	{
		real32 me[4];
		struct
		{
			real32 x,y,z,w;
		};

		struct
		{
			real32 r,g,b,a;
		};
	};

	vec4f& set(real32 _x,real32 _y,real32 _z, real32 _w);
	vec4f& set(const vec4f& b);
	vec4f& set(real32 _x);

	// piecewise operators
	bool operator==(const vec4f& rh) const;
	bool operator!=(const vec4f& rh) const;

	vec4f& operator*=(const vec4f& rh);
	vec4f& operator/=(const vec4f& rh);
	vec4f& operator+=(const vec4f& rh);
	vec4f& operator-=(const vec4f& rh);

	vec4f operator*(const vec4f& rh) const;
	vec4f operator/(const vec4f& rh) const;
	vec4f operator+(const vec4f& rh) const;
	vec4f operator-(const vec4f& rh) const;

	vec4f operator-() const;
	vec4f operator+() const;

	//real32 operators:

	vec4f& operator=(const real32& rh);
	vec4f& operator*=(const real32& rh);
	vec4f& operator/=(const real32& rh);

	vec4f operator*(const real32& rh) const;
	vec4f operator/(const real32& rh) const;
};

inline vec4f _vec4f(real32 x,real32 y,real32 z, real32 w)
{
	vec4f result;
	result.set(x,y,z,w);
	return result;
}

inline vec4f& vec4f::set(real32 _x,real32 _y,real32 _z, real32 _w)
{
	x=_x;y=_y;z=_z;w=_w;
	return *this;
}

inline vec4f& vec4f::set(const vec4f& b)
{
	x=b.x;y=b.y;z=b.z;w=b.w;
	return *this;
}

inline vec4f& vec4f::set(real32 _x)
{
	x=_x;y=_x;z=_x;w=_x;
	return *this;
}

// piecewise operators
inline bool vec4f::operator==(const vec4f& rh) const
{
	return fabs(rh.x-x)<ERROR_MARGIN && fabs(rh.y-y)<ERROR_MARGIN && fabs(rh.z-z)<ERROR_MARGIN;
}
inline bool vec4f::operator!=(const vec4f& rh) const
{
	return fabs(rh.x-x)>=ERROR_MARGIN || fabs(rh.y-y)>=ERROR_MARGIN || fabs(rh.z-z)>=ERROR_MARGIN;
}

inline vec4f& vec4f::operator*=(const vec4f& rh)
{
	x*=rh.x;
	y*=rh.y;
	z*=rh.z;
	return *this;
}

inline vec4f& vec4f::operator/=(const vec4f& rh)
{
	x/=rh.x;
	y/=rh.y;
	z/=rh.z;
	return *this;
}

inline vec4f& vec4f::operator+=(const vec4f& rh)
{
	x+=rh.x;
	y+=rh.y;
	z+=rh.z;
	return *this;
}

inline vec4f& vec4f::operator-=(const vec4f& rh)
{
	x-=rh.x;
	y-=rh.y;
	z-=rh.z;
	return *this;
}

inline vec4f vec4f::operator*(const vec4f& rh) const
{
	vec4f result = {x*rh.x,
					y*rh.y,
					z*rh.z};
	return result;
}

inline vec4f vec4f::operator/(const vec4f& rh) const
{
	vec4f result = {x/rh.x,
					y/rh.y,
					z/rh.z};
	return result;
}

inline vec4f vec4f::operator+(const vec4f& rh) const
{
	vec4f result = {x+rh.x,
					y+rh.y,
					z+rh.z};
	return result;
}

inline vec4f vec4f::operator-(const vec4f& rh) const
{
	vec4f result = {x-rh.x,
					y-rh.y,
					z-rh.z};
	return result;
}

inline vec4f vec4f::operator-() const
{
	vec4f result = {-x,
					-y,
					-z};
	return result;
}

inline vec4f vec4f::operator+() const
{
	return vec4f(*this);
}

//real32 operators:

inline vec4f& vec4f::operator=(const real32& rh)
{
	x=rh;
	y=rh;
	z=rh;
	return *this;
}

inline vec4f& vec4f::operator*=(const real32& rh)
{
	x*=rh;
	y*=rh;
	z*=rh;
	return *this;
}

inline vec4f& vec4f::operator/=(const real32& rh)
{
	x/=rh;
	y/=rh;
	z/=rh;
	return *this;
}

inline vec4f vec4f::operator*(const real32& rh) const
{
	vec4f result = {x*rh,y*rh,z*rh};
	return result;
}

inline vec4f vec4f::operator/(const real32& rh) const
{
	vec4f result = {x/rh,y/rh,z/rh};
	return result;
}

#endif
