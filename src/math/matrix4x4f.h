/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * lbMatrix4x4f.h
 *
 *  Created on: 23-04-2011
 *      Author: Spliter
 */

#ifndef LBMATRIX4X4F_H_
#define LBMATRIX4X4F_H_

#include "vec3f.h"
#include "matrix3x3f.h"

struct matrix4x4f
{
public:
	union
	{
		real32 me[16];
		struct
		{
			real32 m00;
			real32 m10;
			real32 m20;
			real32 m30;

			real32 m01;
			real32 m11;
			real32 m21;
			real32 m31;

			real32 m02;
			real32 m12;
			real32 m22;
			real32 m32;

			real32 m03;
			real32 m13;
			real32 m23;
			real32 m33;
		};
	};


	void set(real32 f);
	void set(	real32 _a, real32 _b, real32 _c, real32 _d,
				real32 _e, real32 _f, real32 _g, real32 _h,
				real32 _i, real32 _j, real32 _k, real32 _l,
				real32 _m, real32 _n, real32 _o, real32 _p);
	void set(const matrix4x4f& mb);
	void set(const matrix3x3f& mb);

	matrix4x4f& makeIdentity();
	matrix4x4f& makeRotation(real32 rad,vec3f axis);
	matrix4x4f& makeRotation(real32 rad,real32 ax,real32 ay,real32 az);
	matrix4x4f& makeRotationDeg(real32 deg, vec3f axis);
	matrix4x4f& makeRotationDeg(real32 deg,real32 ax,real32 ay,real32 az);
	matrix4x4f& makeScale(real32 xscale, real32 yscale, real32 zscale);
	matrix4x4f& makeTranslation(real32 xtranslate,real32 ytranslate, real32 ztranslate);
	matrix4x4f& makeTranslation(vec3f translation);

	matrix4x4f& transpose();
	matrix4x4f transposed() const;

	matrix4x4f& inverse();
	matrix4x4f inversed() const;

	matrix4x4f& rotate(real32 rad,vec3f axis);
	matrix4x4f& rotate(real32 rad, real32 ax, real32 ay, real32 az);
	matrix4x4f rotated(real32 rad,vec3f axis) const;
	matrix4x4f rotated(real32 rad, real32 ax, real32 ay, real32 az) const;
	matrix4x4f& rotateDeg(real32 deg,vec3f axis);
	matrix4x4f& rotateDeg(real32 deg, real32 ax, real32 ay, real32 az);
	matrix4x4f rotatedDeg(real32 deg,vec3f axis) const;
	matrix4x4f rotatedDeg(real32 deg, real32 ax, real32 ay, real32 az) const;

	matrix4x4f& scale(real32 _scale);
	matrix4x4f scaled(real32 _scale) const;
	matrix4x4f& scale(real32 xscale,real32 yscale, real32 zscale);
	matrix4x4f scaled(real32 xscale,real32 yscale, real32 zscale) const;

	matrix4x4f& translate(real32 x,real32 y, real32 z);
	matrix4x4f translated(real32 x,real32 y, real32 z) const;
	matrix4x4f& translate(vec3f translation);
	matrix4x4f translated(vec3f translation) const;


//	matrix4x4f& postMultiply(const matrix4x4f& b);
//	matrix4x4f postMultiplied(const matrix4x4f& b) const;

	matrix4x4f& preMultiply(const matrix4x4f& b);
	matrix4x4f preMultiplied(const matrix4x4f& b) const;

	matrix4x4f& transform(const matrix4x4f& b);
	matrix4x4f transformed(const matrix4x4f& b) const;

	matrix4x4f& operator*=(const matrix4x4f& b);
	matrix4x4f& operator/=(const matrix4x4f& b);
	matrix4x4f& operator+=(const matrix4x4f& b);
	matrix4x4f& operator-=(const matrix4x4f& b);

	matrix4x4f operator*(const matrix4x4f& b) const;
	matrix4x4f operator/(const matrix4x4f& b) const;
	matrix4x4f operator+(const matrix4x4f& b) const;
	matrix4x4f operator-(const matrix4x4f& b) const;

//	matrix4x4f& operator=(real32 f);
	matrix4x4f& operator*=(real32 f);
	matrix4x4f& operator/=(real32 f);

	matrix4x4f operator*(real32 f) const;
	matrix4x4f operator/(real32 f) const;

	vec3f operator*(const vec3f& v) const;
};

matrix4x4f _matrix4x4f(real32 val)
{
	matrix4x4f result;
	result.m00=val;result.m01=val;result.m02=val;result.m03=val;
	result.m10=val;result.m11=val;result.m12=val;result.m13=val;
	result.m20=val;result.m21=val;result.m22=val;result.m23=val;
	result.m30=val;result.m31=val;result.m32=val;result.m33=val;
	return result;
}

matrix4x4f _matrix4x4f(	real32 _a, real32 _b, real32 _c, real32 _d,
			real32 _e, real32 _f, real32 _g, real32 _h,
			real32 _i, real32 _j, real32 _k, real32 _l,
			real32 _m, real32 _n, real32 _o, real32 _p)
{
	matrix4x4f result;
	result.m00=_a;result.m01=_e;result.m02=_i;result.m03=_m;
	result.m10=_b;result.m11=_f;result.m12=_j;result.m13=_n;
	result.m20=_c;result.m21=_g;result.m22=_k;result.m23=_o;
	result.m30=_d;result.m31=_h;result.m32=_l;result.m33=_p;
	return result;
}

matrix4x4f _matrix4x4f(const matrix4x4f& mb)
{
	matrix4x4f result;
	result.m00=mb.m00;result.m01=mb.m01;result.m02=mb.m02;result.m03=mb.m03;
	result.m10=mb.m10;result.m11=mb.m11;result.m12=mb.m12;result.m13=mb.m13;
	result.m20=mb.m20;result.m21=mb.m21;result.m22=mb.m22;result.m23=mb.m23;
	result.m30=mb.m30;result.m31=mb.m31;result.m32=mb.m32;result.m33=mb.m33;
	return result;
}

matrix4x4f _matrix4x4f(const matrix3x3f& mb)
{
	matrix4x4f result;
	result.m00=mb.m00;	result.m01=mb.m01;	result.m02=0;	result.m03=mb.m02;
	result.m10=mb.m10;	result.m11=mb.m11;	result.m12=0;	result.m13=mb.m12;
	result.m20=0;	 	result.m21=0;		result.m22=1;	result.m23=0;
	result.m30=mb.m20;	result.m31=mb.m21;	result.m32=0;	result.m33=mb.m22;
	return result;
}

void matrix4x4f::set(real32 val)
{
	m00=val;m01=val;m02=val;m03=val;
	m10=val;m11=val;m12=val;m13=val;
	m20=val;m21=val;m22=val;m23=val;
	m30=val;m31=val;m32=val;m33=val;
}

void matrix4x4f::set(	real32 _a, real32 _b, real32 _c, real32 _d,
			real32 _e, real32 _f, real32 _g, real32 _h,
			real32 _i, real32 _j, real32 _k, real32 _l,
			real32 _m, real32 _n, real32 _o, real32 _p)
{
	m00=_a;m01=_e;m02=_i;m03=_m;
	m10=_b;m11=_f;m12=_j;m13=_n;
	m20=_c;m21=_g;m22=_k;m23=_o;
	m30=_d;m31=_h;m32=_l;m33=_p;
}

void matrix4x4f::set(const matrix4x4f& mb)
{
	m00=mb.m00;m01=mb.m01;m02=mb.m02;m03=mb.m03;
	m10=mb.m10;m11=mb.m11;m12=mb.m12;m13=mb.m13;
	m20=mb.m20;m21=mb.m21;m22=mb.m22;m23=mb.m23;
	m30=mb.m30;m31=mb.m31;m32=mb.m32;m33=mb.m33;
}

void matrix4x4f::set(const matrix3x3f& mb)
{
	m00=mb.m00;	m01=mb.m01;	m02=0;	m03=mb.m02;
	m10=mb.m10;	m11=mb.m11;	m12=0;	m13=mb.m12;
	m20=0;	 	m21=0;		m22=1;	m23=0;
	m30=mb.m20;	m31=mb.m21;	m32=0;	m33=mb.m22;
}

matrix4x4f& matrix4x4f::makeIdentity()
{
	m00=1;m01=0;m02=0;m03=0;
	m10=0;m11=1;m12=0;m13=0;
	m20=0;m21=0;m22=1;m23=0;
	m30=0;m31=0;m32=0;m33=1;
	return *this;
}

matrix4x4f& matrix4x4f::makeRotation(real32 rad,vec3f axis)
{
	makeRotation(rad,axis.x,axis.y,axis.z);
	return *this;
}

matrix4x4f& matrix4x4f::makeRotation(real32 rad,real32 ax,real32 ay,real32 az)
{
	real32 cs=cos(rad);
	real32 s=sin(rad);
	real32 cc=1.0f-cs;

	m00=cs+ax*ax*cc;	m01=ax*ay*cc-az*s;	m02=ax*az*cc+ay*s;	m03=0;
	m10=ay*ax*cc+az*s;	m11=cs+ay*ay*cc;		m12=ay*az*cc-ax*s;	m13=0;
	m20=az*ax*cc-ay*s;	m21=az*ay*cc+ax*s;	m22=cs+az*az*cc;		m23=0;
	m30=0;				m31=0;				m32=0;				m33=1;
	return *this;
}

matrix4x4f& matrix4x4f::makeRotationDeg(real32 deg,vec3f axis)
{
	makeRotation((real32)(deg*DEG_TO_RAD_CONST),axis.x,axis.y,axis.z);
	return *this;
}

matrix4x4f& matrix4x4f::makeRotationDeg(real32 deg,real32 ax,real32 ay,real32 az)
{
	makeRotation((real32)(deg*DEG_TO_RAD_CONST),ax,ay,az);
	return *this;
}

matrix4x4f& matrix4x4f::makeScale(real32 xscale, real32 yscale, real32 zscale)
{
	m00=xscale;m01=0;	  m02=0;		m03=0;
	m10=0;	 m11=yscale;m12=0;		m13=0;
	m20=0;	 m21=0;	  m22=zscale; m23=0;
	m30=0;	 m31=0;	  m32=0;		m33=1;
	return *this;
}

matrix4x4f& matrix4x4f::makeTranslation(real32 xtranslate,real32 ytranslate, real32 ztranslate)
{
	m00=1;m01=0;m02=0;m03=xtranslate;
	m10=0;m11=1;m12=0;m13=ytranslate;
	m20=0;m21=0;m22=1;m23=ztranslate;
	m30=0;m31=0;m32=0;m33=1;
	return *this;
}

matrix4x4f& matrix4x4f::makeTranslation(vec3f translation)
{
	m00=1;m01=0;m02=0;m03=translation.x;
	m10=0;m11=1;m12=0;m13=translation.y;
	m20=0;m21=0;m22=1;m23=translation.z;
	m30=0;m31=0;m32=0;m33=1;
	return *this;
}

matrix4x4f& matrix4x4f::transpose()
{
	set(transposed());
	return *this;
}
matrix4x4f matrix4x4f::transposed() const
{
	matrix4x4f result = {m00,m01,m02,m03,
						m10,m11,m12,m13,
						m20,m21,m22,m23,
						m30,m31,m32,m33};
	return result;
}

matrix4x4f& matrix4x4f::inverse()
{
	real32 inv[16], det;
    int32 i;

    inv[0] = me[5]  * me[10] * me[15] -
             me[5]  * me[11] * me[14] -
             me[9]  * me[6]  * me[15] +
             me[9]  * me[7]  * me[14] +
             me[13] * me[6]  * me[11] -
             me[13] * me[7]  * me[10];

    inv[4] = -me[4]  * me[10] * me[15] +
              me[4]  * me[11] * me[14] +
              me[8]  * me[6]  * me[15] -
              me[8]  * me[7]  * me[14] -
              me[12] * me[6]  * me[11] +
              me[12] * me[7]  * me[10];

    inv[8] = me[4]  * me[9] * me[15] -
             me[4]  * me[11] * me[13] -
             me[8]  * me[5] * me[15] +
             me[8]  * me[7] * me[13] +
             me[12] * me[5] * me[11] -
             me[12] * me[7] * me[9];

    inv[12] = -me[4]  * me[9] * me[14] +
               me[4]  * me[10] * me[13] +
               me[8]  * me[5] * me[14] -
               me[8]  * me[6] * me[13] -
               me[12] * me[5] * me[10] +
               me[12] * me[6] * me[9];

    inv[1] = -me[1]  * me[10] * me[15] +
              me[1]  * me[11] * me[14] +
              me[9]  * me[2] * me[15] -
              me[9]  * me[3] * me[14] -
              me[13] * me[2] * me[11] +
              me[13] * me[3] * me[10];

    inv[5] = me[0]  * me[10] * me[15] -
             me[0]  * me[11] * me[14] -
             me[8]  * me[2] * me[15] +
             me[8]  * me[3] * me[14] +
             me[12] * me[2] * me[11] -
             me[12] * me[3] * me[10];

    inv[9] = -me[0]  * me[9] * me[15] +
              me[0]  * me[11] * me[13] +
              me[8]  * me[1] * me[15] -
              me[8]  * me[3] * me[13] -
              me[12] * me[1] * me[11] +
              me[12] * me[3] * me[9];

    inv[13] = me[0]  * me[9] * me[14] -
              me[0]  * me[10] * me[13] -
              me[8]  * me[1] * me[14] +
              me[8]  * me[2] * me[13] +
              me[12] * me[1] * me[10] -
              me[12] * me[2] * me[9];

    inv[2] = me[1]  * me[6] * me[15] -
             me[1]  * me[7] * me[14] -
             me[5]  * me[2] * me[15] +
             me[5]  * me[3] * me[14] +
             me[13] * me[2] * me[7] -
             me[13] * me[3] * me[6];

    inv[6] = -me[0]  * me[6] * me[15] +
              me[0]  * me[7] * me[14] +
              me[4]  * me[2] * me[15] -
              me[4]  * me[3] * me[14] -
              me[12] * me[2] * me[7] +
              me[12] * me[3] * me[6];

    inv[10] = me[0]  * me[5] * me[15] -
              me[0]  * me[7] * me[13] -
              me[4]  * me[1] * me[15] +
              me[4]  * me[3] * me[13] +
              me[12] * me[1] * me[7] -
              me[12] * me[3] * me[5];

    inv[14] = -me[0]  * me[5] * me[14] +
               me[0]  * me[6] * me[13] +
               me[4]  * me[1] * me[14] -
               me[4]  * me[2] * me[13] -
               me[12] * me[1] * me[6] +
               me[12] * me[2] * me[5];

    inv[3] = -me[1] * me[6] * me[11] +
              me[1] * me[7] * me[10] +
              me[5] * me[2] * me[11] -
              me[5] * me[3] * me[10] -
              me[9] * me[2] * me[7] +
              me[9] * me[3] * me[6];

    inv[7] = me[0] * me[6] * me[11] -
             me[0] * me[7] * me[10] -
             me[4] * me[2] * me[11] +
             me[4] * me[3] * me[10] +
             me[8] * me[2] * me[7] -
             me[8] * me[3] * me[6];

    inv[11] = -me[0] * me[5] * me[11] +
               me[0] * me[7] * me[9] +
               me[4] * me[1] * me[11] -
               me[4] * me[3] * me[9] -
               me[8] * me[1] * me[7] +
               me[8] * me[3] * me[5];

    inv[15] = me[0] * me[5] * me[10] -
              me[0] * me[6] * me[9] -
              me[4] * me[1] * me[10] +
              me[4] * me[2] * me[9] +
              me[8] * me[1] * me[6] -
              me[8] * me[2] * me[5];

    det = me[0] * inv[0] + me[1] * inv[4] + me[2] * inv[8] + me[3] * inv[12];

    if (det != 0)
    {
		det = 1.0f / det;
		for (i = 0; i < 16; i++)
			me[i] = inv[i] * det;
    }
    else
    {
    	return makeIdentity();
    }

	return *this;
}

matrix4x4f matrix4x4f::inversed() const
{
	matrix4x4f mat(*this);
	mat.inverse();
	return mat;
}

matrix4x4f& matrix4x4f::rotate(real32 rad,vec3f axis)
{
	matrix4x4f mat;
	mat.makeRotation(rad,axis);
	transform(mat);
	return *this;
}

matrix4x4f& matrix4x4f::rotate(real32 rad, real32 ax, real32 ay, real32 az)
{
	matrix4x4f mat;
	mat.makeRotation(rad,ax,ay,az);
	transform(mat);
	return *this;
}

matrix4x4f matrix4x4f::rotated(real32 rad,vec3f axis) const
{
	matrix4x4f mat;
	mat.makeRotation(rad,axis);
	return transformed(mat);
}

matrix4x4f matrix4x4f::rotated(real32 rad, real32 ax, real32 ay, real32 az) const
{
	matrix4x4f mat;
	mat.makeRotation(rad,ax,ay,az);
	return transformed(mat);
}

matrix4x4f& matrix4x4f::rotateDeg(real32 deg,vec3f axis)
{
	return rotate((real32)(deg*DEG_TO_RAD_CONST),axis);
}

matrix4x4f& matrix4x4f::rotateDeg(real32 deg, real32 ax, real32 ay, real32 az)
{
	return rotate((real32)(deg*DEG_TO_RAD_CONST),ax,ay,az);
}

matrix4x4f matrix4x4f::rotatedDeg(real32 deg,vec3f axis) const
{
	return rotated((real32)(deg*DEG_TO_RAD_CONST),axis);
}
matrix4x4f matrix4x4f::rotatedDeg(real32 deg, real32 ax, real32 ay, real32 az) const
{
	return rotated((real32)(deg*DEG_TO_RAD_CONST),ax,ay,az);
}

matrix4x4f& matrix4x4f::scale(real32 _scale)
{
	return scale(_scale,_scale,_scale);
}

matrix4x4f matrix4x4f::scaled(real32 _scale) const
{
	return scaled(_scale,_scale,_scale);
}

matrix4x4f& matrix4x4f::scale(real32 xscale,real32 yscale, real32 zscale)
{
	matrix4x4f mat;
	mat.makeScale(xscale, yscale, zscale);
	return transform(mat);
}

matrix4x4f matrix4x4f::scaled(real32 xscale,real32 yscale, real32 zscale) const
{
	matrix4x4f mat;
	mat.makeScale(xscale, yscale, zscale);
	return transformed(mat);
}

matrix4x4f& matrix4x4f::translate(real32 x,real32 y, real32 z)
{
	matrix4x4f mat;
	mat.makeTranslation(x,y,z);
	return transform(mat);
}

matrix4x4f matrix4x4f::translated(real32 x,real32 y, real32 z) const
{
	matrix4x4f mat;
	mat.makeTranslation(x,y,z);
	return transformed(mat);
}

matrix4x4f& matrix4x4f::translate(vec3f translation)
{
	matrix4x4f mat;
	mat.makeTranslation(translation);
	return transform(mat);
}

matrix4x4f matrix4x4f::translated(vec3f translation) const
{
	matrix4x4f mat;
	mat.makeTranslation(translation);
	return transformed(mat);
}

//matrix4x4f& matrix4x4f::postMultiply(const matrix4x4f& mb)
//{
//
//}
//
//matrix4x4f matrix4x4f::postMultiplied(const matrix4x4f& mb) const
//{
//}

matrix4x4f& matrix4x4f::preMultiply(const matrix4x4f& mb)
{
	set(	m00*mb.m00+m01*mb.m10+m02*mb.m20+m03*mb.m30,
						m10*mb.m00+m11*mb.m10+m12*mb.m20+m13*mb.m30,
						m20*mb.m00+m21*mb.m10+m22*mb.m20+m23*mb.m30,
						m30*mb.m00+m31*mb.m10+m32*mb.m20+m33*mb.m30,

						m00*mb.m01+m01*mb.m11+m02*mb.m21+m03*mb.m31,
						m10*mb.m01+m11*mb.m11+m12*mb.m21+m13*mb.m31,
						m20*mb.m01+m21*mb.m11+m22*mb.m21+m23*mb.m31,
						m30*mb.m01+m31*mb.m11+m32*mb.m21+m33*mb.m31,

						m00*mb.m02+m01*mb.m12+m02*mb.m22+m03*mb.m32,
						m10*mb.m02+m11*mb.m12+m12*mb.m22+m13*mb.m32,
						m20*mb.m02+m21*mb.m12+m22*mb.m22+m23*mb.m32,
						m30*mb.m02+m31*mb.m12+m32*mb.m22+m33*mb.m32,

						m00*mb.m03+m01*mb.m13+m02*mb.m23+m03*mb.m33,
						m10*mb.m03+m11*mb.m13+m12*mb.m23+m13*mb.m33,
						m20*mb.m03+m21*mb.m13+m22*mb.m23+m23*mb.m33,
						m30*mb.m03+m31*mb.m13+m32*mb.m23+m33*mb.m33);
	return *this;
}

matrix4x4f matrix4x4f::preMultiplied(const matrix4x4f& mb) const
{
	matrix4x4f result = {m00*mb.m00+m01*mb.m10+m02*mb.m20+m03*mb.m30,
						m10*mb.m00+m11*mb.m10+m12*mb.m20+m13*mb.m30,
						m20*mb.m00+m21*mb.m10+m22*mb.m20+m23*mb.m30,
						m30*mb.m00+m31*mb.m10+m32*mb.m20+m33*mb.m30,

						m00*mb.m01+m01*mb.m11+m02*mb.m21+m03*mb.m31,
						m10*mb.m01+m11*mb.m11+m12*mb.m21+m13*mb.m31,
						m20*mb.m01+m21*mb.m11+m22*mb.m21+m23*mb.m31,
						m30*mb.m01+m31*mb.m11+m32*mb.m21+m33*mb.m31,

						m00*mb.m02+m01*mb.m12+m02*mb.m22+m03*mb.m32,
						m10*mb.m02+m11*mb.m12+m12*mb.m22+m13*mb.m32,
						m20*mb.m02+m21*mb.m12+m22*mb.m22+m23*mb.m32,
						m30*mb.m02+m31*mb.m12+m32*mb.m22+m33*mb.m32,

						m00*mb.m03+m01*mb.m13+m02*mb.m23+m03*mb.m33,
						m10*mb.m03+m11*mb.m13+m12*mb.m23+m13*mb.m33,
						m20*mb.m03+m21*mb.m13+m22*mb.m23+m23*mb.m33,
						m30*mb.m03+m31*mb.m13+m32*mb.m23+m33*mb.m33};
	return result;
}

matrix4x4f& matrix4x4f::transform(const matrix4x4f& mb)
{
	return preMultiply(mb);
}

matrix4x4f matrix4x4f::transformed(const matrix4x4f& mb) const
{
	return preMultiplied(mb);
}

matrix4x4f& matrix4x4f::operator*=(const matrix4x4f& mb)
{
	preMultiply(mb);
	return *this;
}

matrix4x4f matrix4x4f::operator*(const matrix4x4f& mb) const
{
	return preMultiplied(mb);
}


matrix4x4f& matrix4x4f::operator*=(real32 val)
{
	m00*=val;m01*=val;m02*=val;m03*=val;
	m10*=val;m11*=val;m12*=val;m13*=val;
	m20*=val;m21*=val;m22*=val;m23*=val;
	m30*=val;m31*=val;m32*=val;m33*=val;
	return *this;
}

matrix4x4f& matrix4x4f::operator/=(real32 val)
{
	m00/=val;m01/=val;m02/=val;m03/=val;
	m10/=val;m11/=val;m12/=val;m13/=val;
	m20/=val;m21/=val;m22/=val;m23/=val;
	m30/=val;m31/=val;m32/=val;m33/=val;
	return *this;
}

matrix4x4f matrix4x4f::operator*(real32 val) const
{
	matrix4x4f mat=*this;
	mat*=val;
	return mat;
}

matrix4x4f matrix4x4f::operator/(real32 val) const
{
	matrix4x4f mat=*this;
	mat/=val;
	return mat;
}

vec3f matrix4x4f::operator*(const vec3f& v) const
{
	vec3f result = {	m00*v.x+m01*v.y+m02*v.z+m03*1,
						m10*v.x+m11*v.y+m12*v.z+m13*1,
						m20*v.x+m21*v.y+m22*v.z+m23*1};
	return result;
}

#endif /* LBMATRIX4X4F_H_ */
