/*
 * map.cpp
 *
 *  Created on: 27 Dec 2015
 *      Author: MikoKuta
 */


#include "map.h"

renderer_mesh_state generateTileMapMesh(game_renderer *renderer, memory_manager *memory, map_info *map, tile_set_info *tileSet, float tileSize)
{
	renderer_mesh_state result = { };
	result = renderer->allocStaticMesh(memory, map->width * map->height * 6, tileSet->tileset, true, RendererPosition2D, RendererColorNone, RendererTexCoords2D);

	struct tileVert
	{
		vec2f pos;
		vec2f uv;
	};
	int32 stride = sizeof(tileVert) / sizeof(real32);

	vec2f tilesetScale = _vec2f(1.0f / tileSet->width, 1.0f / tileSet->height);
	box2f tileBox = _box2f(0, 0, tilesetScale.x, tilesetScale.y);

	int32 mapWidth = map->width;
	int32 mapHeight = map->height;

	for (int32 cellY = 0; cellY < mapHeight; cellY++)
	{
		for (int32 cellX = 0; cellX < mapWidth; cellX++)
		{
			int32 id = cellX + cellY * mapWidth;

			int foundCellTypeIndex = -1;
			for(int32 cellTypeIndex = 0; cellTypeIndex<tileSet->typesSize; cellTypeIndex++)
			{
				if(map->mapData[id]==tileSet->types[cellTypeIndex].type)
				{
					foundCellTypeIndex = cellTypeIndex;
					break;
				}
			}


			if (foundCellTypeIndex!=0)
			{
				tile_coords tileCoord = { };

				tileCoord.x = tileSet->types[foundCellTypeIndex].info.x;
				tileCoord.y = tileSet->types[foundCellTypeIndex].info.y;

				vec2f tilePos = _vec2f(tileCoord.x * tilesetScale.x, tileCoord.y * tilesetScale.y);
				box2f curTileBox = tileBox + tilePos;
				real32 x1 = ((real32) cellX * tileSize);
				real32 x2 = ((real32) x1 + tileSize);
				real32 y1 = ((real32) cellY * tileSize);
				real32 y2 = ((real32) y1 + tileSize);

				int32 vertId = result.usedVertexCount;
				tileVert *vert = ((tileVert*) (result.vertices)) + vertId;

				vert[0].pos.set(x1, y1);
				vert[0].uv.set(curTileBox.minX, curTileBox.minY);

				vert[1].pos.set(x1, y2);
				vert[1].uv.set(curTileBox.minX, curTileBox.maxY);

				vert[2].pos.set(x2, y2);
				vert[2].uv.set(curTileBox.maxX, curTileBox.maxY);

				vert[3].pos.set(x1, y1);
				vert[3].uv.set(curTileBox.minX, curTileBox.minY);

				vert[4].pos.set(x2, y2);
				vert[4].uv.set(curTileBox.maxX, curTileBox.maxY);

				vert[5].pos.set(x2, y1);
				vert[5].uv.set(curTileBox.maxX, curTileBox.minY);
				result.usedVertexCount += 6;
			}
		}
	}
	return result;
}



bool32 hasCell(map_cell cellType, box2f bbox, map_info map)
{
	real32 tileSize = map.tileSize;
	//bbox.move(-map.position.x, -map.position.y);
	int32 minX = maxi((int32) floor(bbox.minX / tileSize), 0);
	int32 minY = maxi((int32) floor(bbox.minY / tileSize), 0);
	int32 maxX = mini((int32) ceil(bbox.maxX / tileSize), map.width - 1);
	int32 maxY = mini((int32) ceil(bbox.maxY / tileSize), map.height - 1);

	map_cell* cells = map.mapData;
	for (int32 tileY = minY; tileY <= maxY; tileY++)
	{
		for (int32 tileX = minX; tileX <= maxX; tileX++)
		{
			int32 tileId = tileX + tileY * map.width;
			if (cells[tileId] == cellType)
			{
				box2f tileBbox;
				tileBbox.minX = (real32) tileX * tileSize;
				tileBbox.minY = (real32) tileY * tileSize;
				tileBbox.maxX = (real32) tileX * tileSize + tileSize;
				tileBbox.maxY = (real32) tileY * tileSize + tileSize;

				if (bbox.intersects(tileBbox))
				{
					return true;
				}
			}
		}
	}
	return false;
}

map_cell getCell(real32 x, real32 y, map_info map)
{
	real32 tileSize = map.tileSize;
	vec2f pos = { x, y };
//	pos -= map.position;

	int cellX = (int32) floor(pos.x / tileSize);
	int cellY = (int32) floor(pos.y / tileSize);

	if (cellX >= 0 && cellY >= 0 && cellX < map.width && cellY < map.height)
	{
		int32 cellId = cellX + cellY * map.width;
		return map.mapData[cellId];
	}
	return true;
}

map_range getMapRange(box2f bbox, map_info map)
{
	map_range result;
	real32 tileSize = map.tileSize;
//	bbox.move(-map.position.x, -map.position.y);
	result.minX = maxi((int32) floor(bbox.minX / tileSize), 0);
	result.minY = maxi((int32) floor(bbox.minY / tileSize), 0);
	result.maxX = mini((int32) ceil(bbox.maxX / tileSize), map.width - 1);
	result.maxY = mini((int32) ceil(bbox.maxY / tileSize), map.height - 1);
	return result;
}

movement_result testHorizontalMovementCollision(real32 horizontalMovement, box2f bbox, box2f otherBbox)
{
	movement_result result = { horizontalMovement, false, 0 };

	box2f moveBbox = bbox + _vec2f(horizontalMovement, 0);

	if (moveBbox.intersects(otherBbox))
	{
		result.hasCollided = true;

		if (horizontalMovement > 0)
		{
			result.maxAllowedMovement = otherBbox.minX - bbox.maxX;
			result.collisionAxis = COLLISION_AXIS_RIGHT;
		}
		else if (horizontalMovement < 0)
		{
			result.maxAllowedMovement = otherBbox.maxX - bbox.minX;
			result.collisionAxis = COLLISION_AXIS_LEFT;
		}
		else
		{
			result.maxAllowedMovement = 0;
			result.collisionAxis = 0;
		}
	}

	return result;
}

movement_result testVerticalMovementCollision(real32 verticalMovement, box2f bbox, box2f otherBbox)
{
	movement_result result = { verticalMovement, false, 0 };

	box2f moveBbox = bbox + _vec2f(0, verticalMovement);

	if (moveBbox.intersects(otherBbox))
	{
		result.hasCollided = true;

		if (verticalMovement > 0)
		{
			result.maxAllowedMovement = otherBbox.minY - bbox.maxY;
			result.collisionAxis = COLLISION_AXIS_BOTTOM;
		}
		else if (verticalMovement < 0)
		{
			result.maxAllowedMovement = otherBbox.maxY - bbox.minY;
			result.collisionAxis = COLLISION_AXIS_TOP;
		}
		else
		{
			result.maxAllowedMovement = 0;
			result.collisionAxis = 0;
		}
	}

	return result;
}

movement_result getMaxHorizontalMovement(real32 horizontalMovement, box2f worldSpaceEntityBbox, map_info map, map_cell checkCell, bool32 cellIsSolid)
{
	real32 collisionEpsilon = map.tileSize/100.0f;
	movement_result result = { horizontalMovement, false, 0 };
	real32 tileSize = map.tileSize;

	box2f mapSpaceEntityBbox = worldSpaceEntityBbox;
	box2f moveBbox = mapSpaceEntityBbox.getUnion(mapSpaceEntityBbox + _vec2f(result.maxAllowedMovement, 0));

	moveBbox.minY += collisionEpsilon;
	moveBbox.maxY -= collisionEpsilon;

	if (horizontalMovement > 0)
	{
		moveBbox.minX += collisionEpsilon;
	}
	else
	{
		moveBbox.maxX -= collisionEpsilon;
	}

	int32 mapWidth = map.width;
	int32 mapHeight = map.height;

	int32 minX = maxi((int32) floor(moveBbox.minX / tileSize), 0);
	int32 minY = maxi((int32) floor(moveBbox.minY / tileSize), 0);
	int32 maxX = mini((int32) ceil(moveBbox.maxX / tileSize), mapWidth - 1);
	int32 maxY = mini((int32) ceil(moveBbox.maxY / tileSize), mapHeight - 1);

	for (int32 tileY = minY; tileY <= maxY; tileY++)
	{
		real32 tilePosY = tileY * tileSize;
		for (int32 tileX = minX; tileX <= maxX; tileX++)
		{
			real32 tilePosX = tileX * tileSize;
			int32 tileId = tileX + tileY * mapWidth;
			if ((map.mapData[tileId] == checkCell)==cellIsSolid)
			{
				box2f tileBbox;
				//tileBbox.set(_vec2f(tileX*tileSize,tileY*tileSize),_vec2f(tileSize,tileSize),_vec2f(0,0));
				tileBbox.minX = (real32) tilePosX;
				tileBbox.minY = (real32) tilePosY;
				tileBbox.maxX = (real32) tilePosX + tileSize;
				tileBbox.maxY = (real32) tilePosY + tileSize;

				if (moveBbox.intersects(tileBbox))
				{
					if (result.maxAllowedMovement > 0)
					{
						if (moveBbox.maxX > tileBbox.minX)
						{
							moveBbox.maxX = tileBbox.minX;
							result.hasCollided = true;
							result.maxAllowedMovement = maxf(0, tileBbox.minX - mapSpaceEntityBbox.maxX);
							result.collisionAxis = COLLISION_AXIS_RIGHT;
						}
					}
					else if (result.maxAllowedMovement < 0)
					{
						if (moveBbox.minX < tileBbox.maxX)
						{
							moveBbox.minX = tileBbox.maxX;
							result.hasCollided = true;
							result.maxAllowedMovement = minf(0, tileBbox.maxX - mapSpaceEntityBbox.minX);
							result.collisionAxis = COLLISION_AXIS_LEFT;
						}
					}
				}
			}
		}
	}

	return result;
}

movement_result getMaxVerticalMovement(real32 verticalMovement, box2f worldSpaceEntityBbox, map_info map, map_cell checkCell, bool32 cellIsSolid)
{
	real32 collisionEpsilon = map.tileSize/100.0f;
	movement_result result = { verticalMovement, false, 0 };
	real32 tileSize = map.tileSize;
	box2f mapSpaceEntityBbox = worldSpaceEntityBbox;//.moved(-mapX, -mapY);
	box2f moveBbox = mapSpaceEntityBbox.getUnion(mapSpaceEntityBbox + _vec2f(0, result.maxAllowedMovement));

	moveBbox.minX += collisionEpsilon;
	moveBbox.maxX -= collisionEpsilon;

	if (verticalMovement > 0)
	{
		moveBbox.minY += collisionEpsilon;
	}
	else
	{
		moveBbox.maxY -= collisionEpsilon;
	}

	int32 mapWidth = map.width;
	int32 mapHeight = map.height;

	int32 minX = maxi((int32) floor(moveBbox.minX / tileSize), 0);
	int32 minY = maxi((int32) floor(moveBbox.minY / tileSize), 0);
	int32 maxX = mini((int32) ceil(moveBbox.maxX / tileSize), mapWidth - 1);
	int32 maxY = mini((int32) ceil(moveBbox.maxY / tileSize), mapHeight - 1);

	for (int32 tileY = minY; tileY <= maxY; tileY++)
	{
		real32 tilePosY = tileY * tileSize;
		for (int32 tileX = minX; tileX <= maxX; tileX++)
		{
			real32 tilePosX = tileX * tileSize;
			int32 tileId = tileX + tileY * mapWidth;
			if ((map.mapData[tileId] == checkCell) == cellIsSolid)
			{
				box2f tileBbox;
				tileBbox.minX = (real32) tilePosX;
				tileBbox.minY = (real32) tilePosY;
				tileBbox.maxX = (real32) tilePosX + tileSize;
				tileBbox.maxY = (real32) tilePosY + tileSize;

				if (moveBbox.intersects(tileBbox))
				{
					if (result.maxAllowedMovement > 0)
					{
						if (moveBbox.maxY > tileBbox.minY)
						{
							moveBbox.maxY = tileBbox.minY;
							result.hasCollided = true;
							result.maxAllowedMovement = maxf(0, tileBbox.minY - mapSpaceEntityBbox.maxY);
							result.collisionAxis = COLLISION_AXIS_BOTTOM;
						}
					}
					else if (result.maxAllowedMovement < 0)
					{
						if (moveBbox.minY < tileBbox.maxY)
						{
							moveBbox.minY = tileBbox.maxY;
							result.hasCollided = true;
							result.maxAllowedMovement = minf(0, tileBbox.maxY - mapSpaceEntityBbox.minY);
							result.collisionAxis = COLLISION_AXIS_TOP;
						}
					}
				}
			}
		}
	}
	return result;
}

