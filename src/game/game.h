/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * game_common.h
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */

#ifndef GAME_COMMON_H
#define GAME_COMMON_H
#include "../shared/types.h"
#include "../shared/utils.h"

struct rectf
{
	real32 x1,y1,x2,y2;

	rectf& set(real32 _x1, real32 _y1, real32 _x2, real32 _y2)
	{
		x1 = _x1;
		y1 = _y1;
		x2 = _x2;
		y2 = _y2;
		return *this;
	}
};


#include "../shared/renderer.h"
#include "../shared/font_manager.h"
#include "../shared/input.h"

struct game_memory
{
	bool32 isInitialized;
	uint64 gameMemorySize;//in bytes
	uint8* gameMemory;
	game_renderer* renderer;
	game_renderer* debugRenderer;
	font_manager* fontManager;
	void (*platformSetCursorPos)(uint32 cursorX, uint32 cursorY);
};

struct game_screen
{
	int32 width,height;
};

#define GAME_UPDATE_AND_RENDER(name) void name(game_memory* memory, game_screen* screen, game_input* input, real32 frameDif)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);
#define GAME_UPDATE_AND_RENDER_NAME gameUpdateAndRender

#endif
