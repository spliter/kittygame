/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
/*
 * game.cpp
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */
#ifndef GAME_CPP
#define GAME_CPP
#include "game.h"
#include "camera.h"
#include "../shared/resources.h"
#include "../math/box2f.h"
#include "../math/mathUtils.h"
#include "../math/mathUtils.cpp"
#include "../math/random.h"
#include "../math/vec2f.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "map.h"
#include "map.cpp"

enum facing_dir
{
	fdLeft, fdRight
};

uint32 MapWidth = 16;
uint32 MapHeight = 8;
uint8 map[] =
		"1111111111111111"
		"1000000000000001"
		"1000000000000001"
		"1000000000000011"
		"1000001110000111"
		"1000000000011111"
		"1000000000000001"
		"1111111111111111";

struct player_state
{
	vec2f pos;
	vec2f speed;
	vec2f prevAccel;
	facing_dir facingDir;
	bool32 isOnGround;
	image_handle sprite;
};

struct map_state
{
	int width;
	int height;
	uint8 *cells;
	renderer_mesh_state mesh;
};

struct game_state
{
	font_handle debugFont;
	memory_manager gameMemory;
	random_state random;
	vec2f prevMousePos;
	game_camera_2d worldCamera;
	game_camera_2d uiCamera;
	image_handle kittySprite;

	player_state player;
	map_state map;
};

struct sprite_info
{
	int32 frameCount;
	image_handle image;
};

void drawSprite(game_renderer* renderer, image_handle image, real32 x1, real32 y1, real32 x2, real32 y2, int32 frame, int32 frameCount, real32 r, real32 g, real32 b, real32 a, bool32 flipHor = false)
{
	real32 frameTexWidth = (real32) 1.0f / (real32) frameCount;
	real32 u1 = frameTexWidth * frame;
	real32 v1 = 0;
	real32 u2 = frameTexWidth * (frame + 1);
	real32 v2 = 1;

	renderer_mesh_state spriteMesh = renderer->allocTemporaryMesh(6, image, false, RendererPosition2D, RendererColorNone, RendererTexCoords2D);

	spriteMesh.color.set(r, g, b, a);
	spriteMesh.usedVertexCount = 6;

	spriteMesh.pos2f[0].set(x1, y1);
	spriteMesh.pos2f[1].set(x1, y2);
	spriteMesh.pos2f[2].set(x2, y2);

	spriteMesh.pos2f[3].set(x1, y1);
	spriteMesh.pos2f[4].set(x2, y2);
	spriteMesh.pos2f[5].set(x2, y1);

	if (flipHor)
	{
		spriteMesh.texCoords2f[0].set(u2, v1);
		spriteMesh.texCoords2f[1].set(u2, v2);
		spriteMesh.texCoords2f[2].set(u1, v2);
		spriteMesh.texCoords2f[3].set(u2, v1);
		spriteMesh.texCoords2f[4].set(u1, v2);
		spriteMesh.texCoords2f[5].set(u1, v1);
	}
	else
	{
		spriteMesh.texCoords2f[0].set(u1, v1);
		spriteMesh.texCoords2f[1].set(u1, v2);
		spriteMesh.texCoords2f[2].set(u2, v2);

		spriteMesh.texCoords2f[3].set(u1, v1);
		spriteMesh.texCoords2f[4].set(u2, v2);
		spriteMesh.texCoords2f[5].set(u2, v1);
	}
	renderer->drawMesh(spriteMesh);
}

GAME_UPDATE_AND_RENDER(GAME_UPDATE_AND_RENDER_NAME)
{
	game_state *gameState = (game_state*) (memory->gameMemory);
	game_renderer *renderer = memory->renderer;
	game_renderer *debugRenderer = memory->debugRenderer;
	font_manager *fontManager = memory->fontManager;

	if (!memory->isInitialized)
	{
		memory->isInitialized = true;

		gameState->gameMemory.memory = memory->gameMemory + sizeof(game_state);
		gameState->gameMemory.memorySize = memory->gameMemorySize - sizeof(game_state);
		gameState->gameMemory.used = 0;

		gameState->debugFont = fontManager->getFont(Font::TestFont);

		gameState->uiCamera.anchor.set(0.0f, 0.0f);
		gameState->uiCamera.zoom = 1.0f;
		gameState->uiCamera.viewport.x1 = 0;
		gameState->uiCamera.viewport.x2 = (real32) screen->width;
		gameState->uiCamera.viewport.y1 = 0;
		gameState->uiCamera.viewport.y2 = (real32) screen->height;

		gameState->worldCamera.anchor.set(0.5f, 0.5f);
		gameState->worldCamera.zoom = 32.0f;
		gameState->worldCamera.viewport.x1 = 0;
		gameState->worldCamera.viewport.x2 = (real32) screen->width;
		gameState->worldCamera.viewport.y1 = 0;
		gameState->worldCamera.viewport.y2 = (real32) screen->height;

		gameState->kittySprite = renderer->getImage(Image::Kitty);

		gameState->player.pos.set(8,4);

		gameState->map.width = MapWidth;
		gameState->map.height = MapHeight;
		int32 cellCount = gameState->map.width * gameState->map.height;

		gameState->map.cells = gameState->gameMemory.pushStructArray<uint8>(cellCount);
		gameState->map.mesh = renderer->allocStaticMesh(&gameState->gameMemory, cellCount * 6, 0, true, RendererPosition2D, RendererColorRGB, RendererTexCoordsNone, false);

		memcpy(gameState->map.cells, map, sizeof(uint8) * cellCount);

		struct mesh_vert
		{
			vec2f pos;
			vec3f color;
		};

		gameState->map.mesh.usedVertexCount = gameState->map.mesh.totalVertexCount;
		int32 cellIndex = 0;
		for (mesh_vert* vert = (mesh_vert*) gameState->map.mesh.vertices; vert < ((mesh_vert*) gameState->map.mesh.vertices) + gameState->map.mesh.totalVertexCount; vert += 6, cellIndex++)
		{
			int32 x = cellIndex % gameState->map.width;
			int32 y = cellIndex / gameState->map.width;
			vec2f pos = { (real32) x, (real32) y };
			vec3f color;
			if (gameState->map.cells[cellIndex] == '1')
			{
				color.set(0.0f, 0.0f, 0.0f);
			}
			else
			{
				color.set(0.5f, 0.5f, 1.0f);
			}
			vert[0].pos = pos;
			vert[0].color = color;
			vert[1].pos = pos + _vec2f(0.0f, 1.0f);
			vert[1].color = color;
			vert[2].pos = pos + _vec2f(1.0f, 0.0f);
			vert[2].color = color;

			vert[3].pos = pos + _vec2f(0.0f, 1.0f);
			vert[3].color = color;
			vert[4].pos = pos + _vec2f(1.0f, 1.0f);
			vert[4].color = color;
			vert[5].pos = pos + _vec2f(1.0f, 0.0f);
			vert[5].color = color;
		}
	}

	real32 worldLeft = (real32) -screen->width * 0.5f;
	real32 worldTop = (real32) -screen->height * 0.5f;
	real32 worldRight = worldLeft + screen->width;
	real32 worldBottom = worldTop + screen->height;

	gameState->worldCamera.startCamera(debugRenderer);

//Update
	player_state* player = &gameState->player;

	vec2f movement = { };
	real32 movementSpeed = 3.0f;
	if (input->buttonMoveRight.isDown)
	{
		movement.x += movementSpeed;
		player->facingDir = fdRight;
	}
	else if (input->buttonMoveLeft.isDown)
	{
		movement.x -= movementSpeed;
		player->facingDir = fdLeft;
	}

	if (input->buttonJump.wasPressed() && player->isOnGround)
	{
		player->speed.y = -7.0f;
		player->isOnGround = false;
	}

	real32 horFriction = 7.0f;
	real32 verFriction = 1.0f;

	player->pos += player->speed * frameDif * 0.5f + player->prevAccel * 0.5f * frameDif * frameDif;
	player->speed += movement;
	player->prevAccel.x = 0.0f;
	player->prevAccel.y = 10.0f;
	player->prevAccel.x += -(player->speed.x) * horFriction;
	player->prevAccel.y += -(player->speed.y) * verFriction;
	player->speed += player->prevAccel * frameDif;
	if (fabs(player->speed.x) < 0.1f)
	{
		player->speed.x = 0.0f;
	}
	player->pos += player->speed * frameDif * 0.5f + player->prevAccel * 0.5f * frameDif * frameDif;

	int32 playerFrame = 0;
	int32 playerMapX = (int32)player->pos.x;
	int32 playerMapY = (int32)player->pos.y;
	if(playerMapX>=0 && playerMapY>=0 && playerMapX<gameState->map.width && playerMapY<gameState->map.height)
	{
		if (gameState->map.cells[playerMapX+playerMapY*gameState->map.width]=='1')
		{

			if (player->speed.y > 0.0f)
			{
				player->pos.y = (real32)playerMapY;
				//player->pos.y = 0.0f;
				player->speed.y = 0.0f;
				player->isOnGround = true;
			}
		}
	}

	if (player->isOnGround)
	{
		if (player->speed.x > 0.1f || player->speed.x < -0.1f)
		{
			playerFrame = 1;
		}
		else
		{
			playerFrame = 0;
		}
	}
	else
	{
		if (player->speed.y > 0.01f)
		{
			playerFrame = 3;
		}
		else if (player->speed.y < -0.01f)
		{
			playerFrame = 2;
		}
		else
		{
			playerFrame = 1;
		}
	}

	gameState->worldCamera.pos = gameState->player.pos;
//Render
	gameState->uiCamera.viewport.set(0, 0, (real32) screen->width, (real32) screen->height);
	gameState->worldCamera.viewport.set(0, 0, (real32) screen->width, (real32) screen->height);

	gameState->worldCamera.startCamera(renderer);
	renderer->clearView(1.0f, 1.0f, 1.0f);

	renderer->drawMesh(gameState->map.mesh);
//renderer->drawCircle(player->pos.x,player->pos.y,1,16,false,0.0f,0.0f,0.0f,1.0f);

	vec2f mouseWorldSpace = gameState->worldCamera.cameraToWorldSpace(input->mouseX, input->mouseY);
	vec2f mouseCameraSpace = gameState->worldCamera.worldToCameraSpace(mouseWorldSpace.x, mouseWorldSpace.y);
	renderer->drawCircle(mouseWorldSpace.x, mouseWorldSpace.y, 0.1f, 16, true, 1.0f, 0.0f, 0.0f, 1.0f);

	bool32 flipPlayerSprite = false;
	int32 kittySpriteFrameCount = 4;

	flipPlayerSprite = player->facingDir == fdLeft;

	drawSprite(renderer, gameState->kittySprite,
			player->pos.x - 1.0f, player->pos.y - 2.0f,
			player->pos.x + 1.0f, player->pos.y,
			playerFrame, kittySpriteFrameCount,
			1.0f, 1.0f, 1.0f, 1.0f,
			flipPlayerSprite);

	gameState->uiCamera.startCamera(renderer);

	real32 textXStart = 40;
	real32 textYStart = 40;
	char buff[1024 * 4];

	real32 textBackgroundAlpha = 0.9f;

	sprintf_s(buff, "Test");
	font_render_mesh_state debugTextMesh = fontManager->createTemporaryText(buff,
			textXStart, textYStart,
			32.0f,
			gameState->debugFont,
			1.0f, 0.0f, 0.0f, 1.0f);

	renderer->drawRectangle(
			debugTextMesh.boundingBox.minX, debugTextMesh.boundingBox.minY,
			debugTextMesh.boundingBox.maxX, debugTextMesh.boundingBox.maxY,
			false, 0.0f, 0.0f, 0.0f, textBackgroundAlpha);
	renderer->drawMesh(debugTextMesh.mesh);
	textYStart += debugTextMesh.boundingBox.getHeight();

	renderer->drawCircle(mouseCameraSpace.x, mouseCameraSpace.y, 16.0f, 16, true, 1.0f, 1.0f, 0.0f, 1.0f);
}

#endif

