/*
 * map.h
 *
 *  Created on: 27 Dec 2015
 *      Author: MikoKuta
 */

#pragma once
#ifndef MAP_H_
#define MAP_H_

typedef uint8 map_cell;

struct map_info
{
	int32 width;
	int32 height;
	map_cell* mapData;
	real32 tileSize;
	renderer_mesh_state mesh;
};

struct tile_coords
{
	int x, y;
};

struct tile_type_info
{
	map_cell type;
	bool32 checkIfHasValue;
	tile_coords info;
};

struct tile_set_info
{
	int32 width, height;
	tile_type_info* types;
	int32 typesSize;
	image_handle tileset;
};

struct map_range
{
	int minX, minY, maxX, maxY;
};

#define COLLISION_AXIS_LEFT -1
#define COLLISION_AXIS_RIGHT 1
#define COLLISION_AXIS_TOP -1
#define COLLISION_AXIS_BOTTOM 1

struct movement_result
{
	real32 maxAllowedMovement;
	bool32 hasCollided;
	int32 collisionAxis; //-1 if collided to left/up, 1 if collided on right/bottom
};


renderer_mesh_state generateTileMapMesh(game_renderer *renderer, memory_manager *memory, map_info *map, tile_set_info *tileSet, float tileSize);
bool32 hasCell(map_cell cellType, box2f bbox, map_info map);
map_cell getCell(real32 x, real32 y, map_info map);
map_range getMapRange(box2f bbox, map_info map);
movement_result testHorizontalMovementCollision(real32 horizontalMovement, box2f bbox, box2f otherBbox);
movement_result testVerticalMovementCollision(real32 verticalMovement, box2f bbox, box2f otherBbox);
movement_result getMaxHorizontalMovement(real32 horizontalMovement, box2f worldSpaceEntityBbox, map_info map, map_cell checkCell, bool32 cellIsSolid = true);
movement_result getMaxVerticalMovement(real32 verticalMovement, box2f worldSpaceEntityBbox, map_info map, map_cell checkCell , bool32 cellIsSolid = true);

#endif /* MAP_H_ */
