/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * gl_renderer.cpp
 *
 *  Created on: 26 gru 2014
 *      Author: Miko Kuta
 */

#include "../shared/renderer.h"
#include "../shared/utils.h"
#include "../shared/resources.h"
#include "../math/matrix4x4f.h"
#include "../math/quaternionf.h"
#include "../math/box2f.h"
#include "platform_common.h"
#include <math.h>
#include <Windows.h>//have to include this before GL
#include <gl/GL.h>
#include <gl/GLU.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <new.h>

#define _USE_MATH_DEFINES

#define VERTEX_DATA_IS_INTERLACED_FLAG 0x01

#define VERTEX_DATA_POSITION_TYPE_MASK 0x06
#define VERTEX_DATA_POSITION_TYPE_NONE 0x00
#define VERTEX_DATA_POSITION_TYPE_2D   0x02
#define VERTEX_DATA_POSITION_TYPE_3D   0x04

#define VERTEX_DATA_COLOR_TYPE_MASK 0x18
#define VERTEX_DATA_COLOR_TYPE_NONE 0x00
#define VERTEX_DATA_COLOR_TYPE_RGB  0x08
#define VERTEX_DATA_COLOR_TYPE_RGBA 0x10

#define VERTEX_DATA_HAS_TEX_COORDS  0x20

#define VERTEX_DATA_HAS_NORMALS  0x40

struct gl_render_command
{
	enum
	{
		TypeMesh = 0,
		TypeMatrix,
		TypeCamera2D,
		TypeCamera3D,
		TypeClearView,
	};

	int32 type;

	union
	{
		struct
		{
			box2f viewport;
			vec2f centerRatio;
		} camera2D;
		struct
		{
			box2f viewport;
			real32 fov;
			real32 nearClip;
			real32 farClip;
		} camera3D;
		struct
		{
			vec4f color;
		} clear;
		struct
		{
			int32 mode;
			int32 vertCount;
			int32 textureId;
			int32 vertexDataInfo;
			vec4f color;
			union
			{
				struct
				{
					union
					{
						real32 *vertices;
						vec3f *vertices3f;
						vec2f *vertices2f;
					};
					union
					{
						real32 *colors;
						vec3f *colors3f;
						vec4f *colors4f;
					};
					union
					{
						real32 *texCoords;
						vec2f *texCoords2f;
					};
					union
					{
						real32 *normals;
						vec3f *normals3f;
					};
				};
				real32* vertexData;
			};
		} mesh;
		struct
		{
			matrix4x4f matrix;
		} matrix;
	};
};

#define RENDERER_MATRIX_STACK_SIZE 64
#define RENDERER_COMMAND_STACK_SIZE 262144

struct image_state
{
	image_handle image;
	image_info info;
	uint32 glTexture;
	uint64 fileTime;
};

struct gl_renderer_state
{
	memory_manager frameMemory;
	int32 commandCount;
	int32 matrixIndex;
	matrix4x4f matrixStack[RENDERER_MATRIX_STACK_SIZE];
	gl_render_command commandStack[RENDERER_COMMAND_STACK_SIZE];
	image_state imageInfo[Image::Count];
};

#define IMAGE_STRUCTS() IMAGE_DEFS(IMAGE_STRUCT_DEF)
#define IMAGE_STRUCT_DEF(name, filename) filename,

const char* imageFileNames[Image::Count] = { IMAGE_STRUCTS() };

class gl_game_renderer: public game_renderer
{
public:
	static gl_game_renderer* create(uint8* rendererMemory, uint64 rendererMemorySize);

	virtual void setView2D(real32 viewportX, real32 viewportY, real32 viewportWidth, real32 viewportHeight, real32 viewportCenterRatioX, real32 viewportCenterRatioY);
	virtual void setView3D(real32 viewportX, real32 viewportY, real32 viewportWidth, real32 viewportHeight, real32 fov, real32 nearClip, real32 farClip);
	virtual void clearView(real32 r, real32 g, real32 b);

	virtual void setTransform(real32 x, real32 y, real32 scalex, real32 scaley, real32 rotateDeg);
	virtual void mulTransform(real32 x, real32 y, real32 scalex, real32 scaley, real32 rotateDeg);

	virtual void setTransform3D(real32 x, real32 y, real32 z, real32 scalex, real32 scaley, real32 scalez, quaternionf rot);
	virtual void mulTransform3D(real32 x, real32 y, real32 z, real32 scalex, real32 scaley, real32 scalez, quaternionf rot);

	virtual void setMatrix(matrix4x4f* mat);
	virtual void mulMatrix(matrix4x4f* mat);

	virtual void pushMatrix();
	virtual void popMatrix();

	virtual void drawRectangle(real32 x1, real32 y1, real32 x2, real32 y2, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f);
	virtual void drawCircle(real32 x, real32 y, real32 radius, int32 divisions, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f);
	virtual void drawLine(real32 x1, real32 y1, real32 x2, real32 y2, real32 r, real32 g, real32 b, real32 a=1.0f);
	virtual void drawImage(real32 x1, real32 y1, real32 x2, real32 y2, image_handle image, real32 r, real32 g, real32 b, real32 a=1.0f);

	virtual void drawBox(real32 x1, real32 y1, real32 z1, real32 x2, real32 y2, real32 z2, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f);
	virtual void drawSphere(real32 x, real32 y, real32 z, real32 radius, int32 divisions, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f);
	virtual void drawLine(real32 x1, real32 y1, real32 z1, real32 x2, real32 y2, real32 z2, real32 r, real32 g, real32 b, real32 a=1.0f);

	virtual renderer_mesh_state allocTemporaryMesh(
		int32 vertexCount,
		image_handle image,
		bool32 isVertexDataInterlaced,
		renderer_position_type positionType,
		renderer_color_type colorType,
		renderer_tex_coord_type texCoordType,
		bool32 hasNormals);

	virtual renderer_mesh_state allocStaticMesh(
		memory_manager *memoryManager,
		int32 vertexCount,
		image_handle image,
		bool32 isVertexDataInterlaced,
		renderer_position_type positionType,
		renderer_color_type colorType,
		renderer_tex_coord_type texCoordType,
		bool32 hasNormals);

	virtual void drawMesh(renderer_mesh_state meshState);

	virtual image_handle getImage(image_resource image);
	virtual image_info getImageInfo(image_handle image);
	public:
	void resetFrame();
	void drawFrame();
	void checkIfResourcesChanged();
	private:
	void gl_game_renderer::allocVertices(
		gl_render_command* meshCommand,
		int32 vertexCount,
		bool32 interlaced,
		int32 positionDimensions,
		int32 colorDimensions,
		int32 texCoordDimensions,
		int32 normalDimensions);
	gl_render_command* createCommand(int32 type);
	gl_renderer_state* renderState;
};

gl_game_renderer* gl_game_renderer::create(uint8* rendererMemory, uint64 rendererMemorySize)
{

	//char*[] fileNames = ;
	gl_game_renderer* renderer = new (rendererMemory) gl_game_renderer();
	rendererMemory += sizeof(gl_game_renderer);
	rendererMemorySize -= sizeof(gl_game_renderer);

	renderer->renderState = (gl_renderer_state*) rendererMemory;
	rendererMemory += sizeof(gl_renderer_state);
	rendererMemorySize -= sizeof(gl_renderer_state);

	renderer->renderState->frameMemory.memory = rendererMemory;
	renderer->renderState->frameMemory.memorySize = rendererMemorySize;
	renderer->renderState->frameMemory.used = 0;

	ilInit();
	iluInit();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	return renderer;
}

void gl_game_renderer::resetFrame()
{
//	memset(renderState->frameMemory.memory,0,(size_t)renderState->frameMemory.used);
	renderState->frameMemory.used = 0;
	renderState->matrixIndex = 0;
	renderState->commandCount = 0;
	renderState->matrixStack[0].makeIdentity();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void gl_game_renderer::drawFrame()
{
	gl_render_command* command = renderState->commandStack;
	int32 commandCount = renderState->commandCount;

	bool32 prevHadVertices = false;
	bool32 prevHadColors = false;
	bool32 prevHadTexCoords = false;
	bool32 prevHadNormals = false;
	int32 currentTexture = 0;

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, 0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	//TEMP

//	glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
//	real32 diffuse[] = {1.0f,1.0f,1.0f,1.0f};
//	real32 pos[] = {0.0f,0.0f,0.0f,1.0f};
//	glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuse);
//	glLightfv(GL_LIGHT0,GL_POSITION,pos);
//	real32 global_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
//	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
//	glShadeModel(GL_SMOOTH);

//END TEMP

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	GLint viewportParams[4];
	glGetIntegerv(GL_VIEWPORT, viewportParams);
	box2f lastViewport = _box2f(
		(real32) viewportParams[0], (real32) (viewportParams[3] - viewportParams[1]),
		(real32) viewportParams[2], (real32) viewportParams[3]);

	for (int32 commandIndex = 0; commandIndex < commandCount; commandIndex++)
	{
		if (command[commandIndex].type == gl_render_command::TypeMesh)
		{
			int32 vertCount = command[commandIndex].mesh.vertCount;
			int32 textureId = command[commandIndex].mesh.textureId;
			int32 vertexInfo = command[commandIndex].mesh.vertexDataInfo;
			bool32 isInterlaced = vertexInfo & VERTEX_DATA_IS_INTERLACED_FLAG;

			real32 *pos;
			real32 *colors;
			real32 *texCoords;
			real32 *normals;
			int32 positionSize = 0;
			int32 colorSize = 0;
			int32 texCoordSize = 0;
			int32 normalSize = 0;
			int32 stride = 0;

			switch (vertexInfo & VERTEX_DATA_POSITION_TYPE_MASK)
			{
				case VERTEX_DATA_POSITION_TYPE_2D:
				positionSize = 2;
				break;
				case VERTEX_DATA_POSITION_TYPE_3D:
				positionSize = 3;
				break;
			}

			switch (vertexInfo & VERTEX_DATA_COLOR_TYPE_MASK)
			{
				case VERTEX_DATA_COLOR_TYPE_RGB:
				colorSize = 3;
				break;
				case VERTEX_DATA_COLOR_TYPE_RGBA:
				colorSize = 4;
				break;
			}

			if (vertexInfo & VERTEX_DATA_HAS_TEX_COORDS)
			{
				texCoordSize = 2;
			}

			if (vertexInfo & VERTEX_DATA_HAS_NORMALS)
			{
				normalSize = 3;
			}

			if (isInterlaced)
			{
				stride = (positionSize + colorSize + texCoordSize + normalSize)*sizeof(float);
				pos = (real32*) command[commandIndex].mesh.vertexData;
				colors = (real32*) pos + positionSize;
				texCoords = (real32*) colors + colorSize;
				normals = (real32*) texCoords + texCoordSize;
			}
			else
			{
				pos = (real32*) command[commandIndex].mesh.vertices;
				colors = (real32*) command[commandIndex].mesh.colors;
				texCoords = (real32*) command[commandIndex].mesh.texCoords;
				normals = (real32*) command[commandIndex].mesh.normals;
			}

			if (textureId != currentTexture)
			{
				currentTexture = textureId;
				glBindTexture(GL_TEXTURE_2D, currentTexture);
			}

			if (positionSize)
			{
				if (!prevHadVertices)
				{
					glEnableClientState(GL_VERTEX_ARRAY);
				}
				glVertexPointer(positionSize, GL_FLOAT, stride, pos);
			}
			else
			{
				if (prevHadVertices)
				{
					glDisableClientState(GL_VERTEX_ARRAY);
				}
			}

			if (colorSize)
			{
				if (!prevHadColors)
				{
					glEnableClientState(GL_COLOR_ARRAY);
				}
				glColorPointer(colorSize, GL_FLOAT, stride, colors);
			}
			else
			{
				if (prevHadColors)
				{
					glDisableClientState(GL_COLOR_ARRAY);
				}
				glColor4fv(command[commandIndex].mesh.color.me);
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, command[commandIndex].mesh.color.me);
			}

			if (texCoordSize)
			{
				if (!prevHadTexCoords)
				{
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				}
				glTexCoordPointer(texCoordSize, GL_FLOAT, stride, texCoords);
			}
			else
			{
				if (prevHadTexCoords)
				{
					glDisableClientState(GL_TEXTURE_COORD_ARRAY);
				}
			}

			if (normalSize)
			{
				if (!prevHadNormals)
				{
					glEnableClientState(GL_NORMAL_ARRAY);
				}
				glNormalPointer(GL_FLOAT, stride, normals);
			}
			else
			{
				if (prevHadNormals)
				{
					glDisableClientState(GL_NORMAL_ARRAY);
				}
			}

			glDrawArrays(command[commandIndex].mesh.mode, 0, vertCount);

			prevHadVertices = positionSize != 0;
			prevHadColors = colorSize != 0;
			prevHadTexCoords = texCoordSize != 0;
			prevHadNormals = normalSize != 0;
		}
		else if (command[commandIndex].type == gl_render_command::TypeMatrix)
		{
			glLoadMatrixf(command[commandIndex].matrix.matrix.me);
		}
		else if (command[commandIndex].type == gl_render_command::TypeCamera2D)
		{
			box2f viewport = command[commandIndex].camera2D.viewport;
			vec2f centerRatio = command[commandIndex].camera2D.centerRatio;
			glViewport((int32) viewport.minX, (int32) (viewport.getHeight() - viewport.maxY), (int32) (viewport.maxX - viewport.minX), (int32) (viewport.maxY - viewport.minY));
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			real32 width = viewport.getWidth();
			real32 height = viewport.getHeight();
			float left = -(width) * centerRatio.x;
			float top = -(height) * centerRatio.y;
			glOrtho(left, left + width, top + height, top, -2, 2);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			lastViewport = viewport;
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);

		}
		else if (command[commandIndex].type == gl_render_command::TypeCamera3D)
		{
			box2f viewport = command[commandIndex].camera3D.viewport;
			real32 fovRad = degToRad(command[commandIndex].camera3D.fov) / 2;
			real32 nearClip = command[commandIndex].camera3D.nearClip;
			real32 farClip = command[commandIndex].camera3D.farClip;
			real32 width = viewport.getWidth();
			real32 height = viewport.getHeight();
			real32 ratio = 1.0f;
			if (height > 0)
			{
				ratio = width / height;
			};
			glViewport((int32) viewport.minX, (int32) (viewport.getHeight() - viewport.maxY), (int32) (viewport.maxX - viewport.minX), (int32) (viewport.maxY - viewport.minY));
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();

			real32 frustrumWidthScale = sinf(fovRad) * 0.5f * nearClip * ratio;
			real32 frustrumHeightScale = sinf(fovRad) * 0.5f * nearClip;
			glDepthRange(nearClip, farClip);
			glFrustum(
				-frustrumWidthScale,
				frustrumWidthScale,
				-frustrumHeightScale,
				frustrumHeightScale,
				nearClip, farClip);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			lastViewport = viewport;
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
		}
		else if (command[commandIndex].type == gl_render_command::TypeClearView)
		{
			glEnable(GL_SCISSOR_TEST);
			box2f viewport = lastViewport;
			glScissor((int32) viewport.minX, (int32) (viewport.getHeight() - viewport.maxY), (int32) viewport.getWidth(), (int32) viewport.getHeight());
			glClearColor(command[commandIndex].clear.color.r,
				command[commandIndex].clear.color.g,
				command[commandIndex].clear.color.b,
				command[commandIndex].clear.color.a);
			glClearDepth(1.0f);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glDisable(GL_SCISSOR_TEST);
		}
	}

}

//Public functions

gl_render_command* gl_game_renderer::createCommand(int32 type)
{
	gl_render_command* result;
	result = &renderState->commandStack[renderState->commandCount];
	memset(result, 0, sizeof(gl_render_command));
	result->type = type;
	renderState->commandCount++;
	Assert(renderState->commandCount<RENDERER_COMMAND_STACK_SIZE);
	return result;
}

#define RENDERER_NONE    0
#define RENDERER_FLOAT_2 2
#define RENDERER_FLOAT_3 3
#define RENDERER_FLOAT_4 4

void gl_game_renderer::allocVertices(
	gl_render_command* meshCommand,
	int32 vertexCount,
	bool32 interlaced,
	int32 positionDimensions,
	int32 colorDimensions,
	int32 texCoordDimensions,
	int32 normalDimensions)
{

	meshCommand->mesh.vertCount = vertexCount;
	Assert(positionDimensions >= 2 && (colorDimensions >= 3 || colorDimensions == 0) && (texCoordDimensions == 0 || texCoordDimensions == 2) && (normalDimensions == 0 || normalDimensions == 3));

	if (interlaced)
	{
		meshCommand->mesh.vertexData = renderState->frameMemory.pushStructArray<real32>(vertexCount*(positionDimensions+colorDimensions+texCoordDimensions+normalDimensions));
		meshCommand->mesh.vertexDataInfo = VERTEX_DATA_IS_INTERLACED_FLAG;
	}
	else
	{
		meshCommand->mesh.vertices = renderState->frameMemory.pushStructArray<real32>(vertexCount*positionDimensions);
		meshCommand->mesh.colors = colorDimensions ? renderState->frameMemory.pushStructArray<real32>(vertexCount*colorDimensions) : NULL;
		meshCommand->mesh.texCoords = texCoordDimensions ? renderState->frameMemory.pushStructArray<real32>(vertexCount*texCoordDimensions) : NULL;
		meshCommand->mesh.normals = normalDimensions ? renderState->frameMemory.pushStructArray<real32>(vertexCount*normalDimensions) : NULL;
		meshCommand->mesh.vertexDataInfo = 0;
	}

	switch (positionDimensions)
	{
		case 2:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_POSITION_TYPE_2D;
		break;
		case 3:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_POSITION_TYPE_3D;
		break;
	}

	switch (colorDimensions)
	{
		case 3:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_COLOR_TYPE_RGB;
		break;
		case 4:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_COLOR_TYPE_RGBA;
		break;
	}

	if (texCoordDimensions != 0)
	{
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_HAS_TEX_COORDS;
	}

	if (texCoordDimensions != 0)
	{
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_HAS_TEX_COORDS;
	}

	if (normalDimensions != 0)
	{
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_HAS_NORMALS;
	}
}
void gl_game_renderer::setView2D(real32 viewportX, real32 viewportY, real32 viewportWidth, real32 viewportHeight, real32 viewportCenterRatioX, real32 viewportCenterRatioY)
{
	gl_render_command* cameraCommand = createCommand(gl_render_command::TypeCamera2D);
	cameraCommand->camera2D.centerRatio.set(viewportCenterRatioX, viewportCenterRatioY);
	cameraCommand->camera2D.viewport.set(viewportX, viewportY, viewportX + viewportWidth, viewportY + viewportHeight);
}

void gl_game_renderer::setView3D(real32 viewportX, real32 viewportY, real32 viewportWidth, real32 viewportHeight, real32 fov, real32 nearClip, real32 farClip)
{
	gl_render_command* cameraCommand = createCommand(gl_render_command::TypeCamera3D);
	cameraCommand->camera3D.viewport.set(viewportX, viewportY, viewportX + viewportWidth, viewportY + viewportHeight);
	cameraCommand->camera3D.fov = fov;
	cameraCommand->camera3D.nearClip = nearClip;
	cameraCommand->camera3D.farClip = farClip;
}

void gl_game_renderer::clearView(real32 r, real32 g, real32 b)
{
	gl_render_command* clearCommand = createCommand(gl_render_command::TypeClearView);
	clearCommand->clear.color.set(r, g, b, 1.0f);
}

void gl_game_renderer::setTransform(real32 x, real32 y, real32 scalex, real32 scaley, real32 rotateDeg)
{
	matrix4x4f* mat = &renderState->matrixStack[renderState->matrixIndex];
	mat->makeIdentity();
	mat->translate(x, y, 0.0f);
	mat->rotateDeg(rotateDeg, 0.0f, 0.0f, 1.0f);
	mat->scale(scalex, scaley, 1.0f);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::mulTransform(real32 x, real32 y, real32 scalex, real32 scaley, real32 rotateDeg)
{
	matrix4x4f* mat = &renderState->matrixStack[renderState->matrixIndex];
	//mat->makeIdentity();
	mat->translate(x, y, 0.0f);
	mat->rotateDeg(rotateDeg, 0.0f, 0.0f, 1.0f);
	mat->scale(scalex, scaley, 1.0f);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::setTransform3D(real32 x, real32 y, real32 z, real32 scalex, real32 scaley, real32 scalez, quaternionf rot = _quaternionf(0, 0, 0, 1))
{
	matrix4x4f* mat = &renderState->matrixStack[renderState->matrixIndex];
	mat->makeIdentity();
	mat->translate(x, y, z);
	mat->transform(rot.getRotationMatrix());
	mat->scale(scalex, scaley, scalez);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::mulTransform3D(real32 x, real32 y, real32 z, real32 scalex, real32 scaley, real32 scalez, quaternionf rot)
{
	matrix4x4f* mat = &renderState->matrixStack[renderState->matrixIndex];
	mat->translate(x, y, z);
	mat->transform(rot.getRotationMatrix());
	mat->scale(scalex, scaley, scalez);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::setMatrix(matrix4x4f* inMat)
{
	matrix4x4f* mat = &renderState->matrixStack[renderState->matrixIndex];
	mat->set(*inMat);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::mulMatrix(matrix4x4f* inMat)
{
	matrix4x4f* mat = &renderState->matrixStack[renderState->matrixIndex];
	mat->transform(*inMat);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::pushMatrix()
{
	renderState->matrixIndex++;
	Assert(renderState->matrixIndex<RENDERER_MATRIX_STACK_SIZE);
	renderState->matrixStack[renderState->matrixIndex] = renderState->matrixStack[renderState->matrixIndex - 1];
}

void gl_game_renderer::popMatrix()
{
	renderState->matrixIndex--;
	Assert(renderState->matrixIndex >= 0);
	gl_render_command* matrixCommand = createCommand(gl_render_command::TypeMatrix);
	matrixCommand->matrix.matrix = renderState->matrixStack[renderState->matrixIndex];
}

void gl_game_renderer::drawRectangle(real32 x1, real32 y1, real32 x2, real32 y2, bool32 outline, real32 r, real32 g, real32 b, real32 a)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	if (outline)
	{
		allocVertices(meshCommand, 4, false, 2, 0, 0, 0);

		meshCommand->mesh.mode = GL_LINE_LOOP;
		meshCommand->mesh.vertices2f[0].set(x1, y1);
		meshCommand->mesh.vertices2f[1].set(x1, y2);
		meshCommand->mesh.vertices2f[2].set(x2, y2);
		meshCommand->mesh.vertices2f[3].set(x2, y1);

		meshCommand->mesh.color.set(r, g, b, a);
	}
	else
	{
		allocVertices(meshCommand, 4, false, 2, 0, 0, 0);
		meshCommand->mesh.vertexDataInfo = VERTEX_DATA_POSITION_TYPE_2D;

		meshCommand->mesh.mode = GL_TRIANGLE_STRIP;
		meshCommand->mesh.vertices2f[0].set(x1, y1);
		meshCommand->mesh.vertices2f[1].set(x1, y2);
		meshCommand->mesh.vertices2f[2].set(x2, y1);
		meshCommand->mesh.vertices2f[3].set(x2, y2);

		meshCommand->mesh.color.set(r, g, b, a);
	}
}

void gl_game_renderer::drawCircle(real32 x, real32 y, real32 radius, int32 divisions, bool32 outline, real32 r, real32 g, real32 b, real32 a)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	int32 startIndex = 0;
	int32 endIndex = 0;
	if (outline)
	{
		allocVertices(meshCommand, divisions + 1, false, 2, 0, 0, 0);
		meshCommand->mesh.vertexDataInfo = VERTEX_DATA_POSITION_TYPE_2D;
		meshCommand->mesh.mode = GL_LINE_STRIP;
		endIndex = divisions;
	}
	else
	{
		allocVertices(meshCommand, divisions + 2, false, 2, 0, 0, 0);
		meshCommand->mesh.mode = GL_TRIANGLE_FAN;
		startIndex = 1;
		meshCommand->mesh.vertices2f[0].set(x, y);
		endIndex = divisions + 1;
	}

	meshCommand->mesh.color.set(r, g, b, a);
	real32 angleDif = (real32) ((M_PI * 2.0f) / divisions);
	real32 angle = 0;
	for (int32 divisionIndex = startIndex; divisionIndex <= endIndex; divisionIndex++)
	{
		meshCommand->mesh.vertices2f[divisionIndex].set(x + cos(angle) * radius, y - sin(angle) * radius);
		angle += angleDif;
	}
}

void gl_game_renderer::drawLine(real32 x1, real32 y1, real32 x2, real32 y2, real32 r, real32 g, real32 b, real32 a)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	meshCommand->mesh.mode = GL_LINES;
	allocVertices(meshCommand, 2, false, 2, 0, 0, 0);
	meshCommand->mesh.vertices2f[0].set(x1, y1);
	meshCommand->mesh.vertices2f[1].set(x2, y2);
	meshCommand->mesh.color.set(r, g, b, a);
}

void gl_game_renderer::drawImage(real32 x1, real32 y1, real32 x2, real32 y2, int32 image, real32 r, real32 g, real32 b, real32 a)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	allocVertices(meshCommand, 4, false, 2, 0, 2, 0);
	meshCommand->mesh.textureId = renderState->imageInfo[image].glTexture;
	meshCommand->mesh.mode = GL_TRIANGLE_STRIP;

	meshCommand->mesh.vertices2f[0].set(x1, y1);
	meshCommand->mesh.vertices2f[1].set(x1, y2);
	meshCommand->mesh.vertices2f[2].set(x2, y1);
	meshCommand->mesh.vertices2f[3].set(x2, y2);

	meshCommand->mesh.texCoords2f[0].set(0, 0);
	meshCommand->mesh.texCoords2f[1].set(0, 1);
	meshCommand->mesh.texCoords2f[2].set(1, 0);
	meshCommand->mesh.texCoords2f[3].set(1, 1);

	meshCommand->mesh.color.set(r, g, b, a);
}

void gl_game_renderer::drawBox(real32 x1, real32 y1, real32 z1, real32 x2, real32 y2, real32 z2, bool32 outline, real32 r, real32 g, real32 b, real32 a)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	if (outline)
	{
		allocVertices(meshCommand, 16, false, 3, 0, 0, 0);
		meshCommand->mesh.mode = GL_LINE_STRIP;
		meshCommand->mesh.color.set(r, g, b, a);

		vec3f v0 = _vec3f(x1, y1, z1);
		vec3f v1 = _vec3f(x1, y1, z2);
		vec3f v2 = _vec3f(x2, y1, z1);
		vec3f v3 = _vec3f(x2, y1, z2);
		vec3f v4 = _vec3f(x1, y2, z1);
		vec3f v5 = _vec3f(x1, y2, z2);
		vec3f v6 = _vec3f(x2, y2, z1);
		vec3f v7 = _vec3f(x2, y2, z2);

		meshCommand->mesh.vertices3f[0] = v0;
		meshCommand->mesh.vertices3f[1] = v1;
		meshCommand->mesh.vertices3f[2] = v3;
		meshCommand->mesh.vertices3f[3] = v2;
		meshCommand->mesh.vertices3f[4] = v0;

		meshCommand->mesh.vertices3f[5] = v4;
		meshCommand->mesh.vertices3f[6] = v5;
		meshCommand->mesh.vertices3f[7] = v7;
		meshCommand->mesh.vertices3f[8] = v6;

		meshCommand->mesh.vertices3f[9] = v4;
		meshCommand->mesh.vertices3f[10] = v6;
		meshCommand->mesh.vertices3f[11] = v2;
		meshCommand->mesh.vertices3f[12] = v3;
		meshCommand->mesh.vertices3f[13] = v7;
		meshCommand->mesh.vertices3f[14] = v5;
		meshCommand->mesh.vertices3f[15] = v1;
	}
	else
	{
		allocVertices(meshCommand, 36, false, 3, 0, 0, 3);
		meshCommand->mesh.mode = GL_TRIANGLES;
		meshCommand->mesh.color.set(r, g, b, a);

		vec3f v0 = _vec3f(x1, y1, z1);
		vec3f v1 = _vec3f(x1, y1, z2);
		vec3f v2 = _vec3f(x2, y1, z1);
		vec3f v3 = _vec3f(x2, y1, z2);
		vec3f v4 = _vec3f(x1, y2, z1);
		vec3f v5 = _vec3f(x1, y2, z2);
		vec3f v6 = _vec3f(x2, y2, z1);
		vec3f v7 = _vec3f(x2, y2, z2);

		vec3f n0 = _vec3f(0, -1, 0);
		vec3f n1 = _vec3f(0, 0, 1);
		vec3f n2 = _vec3f(0, 1, 0);
		vec3f n3 = _vec3f(0, 0, -1);
		vec3f n4 = _vec3f(-1, 0, 0);
		vec3f n5 = _vec3f(1, 0, 0);

#define SETUP_VERTEX(id,posValue,normalValue) meshCommand->mesh.vertices3f[id]=posValue;meshCommand->mesh.normals3f[id]=normalValue

		SETUP_VERTEX(0, v0, n0);
		SETUP_VERTEX(2, v1, n0);
		SETUP_VERTEX(1, v2, n0);
		SETUP_VERTEX(3, v2, n0);
		SETUP_VERTEX(5, v1, n0);
		SETUP_VERTEX(4, v3, n0);

		SETUP_VERTEX(6, v1, n1);
		SETUP_VERTEX(7, v3, n1);
		SETUP_VERTEX(8, v5, n1);
		SETUP_VERTEX(9, v5, n1);
		SETUP_VERTEX(10, v3, n1);
		SETUP_VERTEX(11, v7, n1);

		SETUP_VERTEX(12, v5, n2);
		SETUP_VERTEX(13, v7, n2);
		SETUP_VERTEX(14, v4, n2);
		SETUP_VERTEX(15, v7, n2);
		SETUP_VERTEX(16, v6, n2);
		SETUP_VERTEX(17, v4, n2);

		SETUP_VERTEX(18, v4, n3);
		SETUP_VERTEX(19, v6, n3);
		SETUP_VERTEX(20, v0, n3);
		SETUP_VERTEX(21, v0, n3);
		SETUP_VERTEX(22, v6, n3);
		SETUP_VERTEX(23, v2, n3);

		SETUP_VERTEX(24, v0, n4);
		SETUP_VERTEX(25, v1, n4);
		SETUP_VERTEX(26, v4, n4);
		SETUP_VERTEX(27, v4, n4);
		SETUP_VERTEX(28, v1, n4);
		SETUP_VERTEX(29, v5, n4);

		SETUP_VERTEX(30, v2, n5);
		SETUP_VERTEX(31, v6, n5);
		SETUP_VERTEX(32, v3, n5);
		SETUP_VERTEX(33, v3, n5);
		SETUP_VERTEX(34, v6, n5);
		SETUP_VERTEX(35, v7, n5);
#undef SETUP_VERTEX
	}
}

void gl_game_renderer::drawSphere(real32 x, real32 y, real32 z, real32 radius, int32 divisions, bool32 outline, real32 r, real32 g, real32 b, real32 a)
{
	//TODO
	Assert(false);
}

void gl_game_renderer::drawLine(real32 x1, real32 y1, real32 z1, real32 x2, real32 y2, real32 z2, real32 r, real32 g, real32 b, real32 a)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	meshCommand->mesh.mode = GL_LINES;
	allocVertices(meshCommand, 2, false, 3, 4, 0, 0);
	meshCommand->mesh.vertices3f[0].set(x1, y1, z1);
	meshCommand->mesh.vertices3f[1].set(x2, y2, z2);
	meshCommand->mesh.colors4f[0].set(r, g, b, a);
	meshCommand->mesh.colors4f[1].set(r, g, b, a);
}

renderer_mesh_state gl_game_renderer::allocTemporaryMesh(
	int32 vertexCount,
	int32 image,
	bool32 isVertexDataInterlaced,
	renderer_position_type positionType,
	renderer_color_type colorType,
	renderer_tex_coord_type texCoordType,
	bool32 hasNormals)
{
	renderer_mesh_state result;
	result.totalVertexCount = vertexCount;
	result.usedVertexCount = 0;
	result.image = image;
	result.color.set(1.0f, 1.0f, 1.0f, 1.0f);

	result.pos = 0;
	result.colors = 0;
	result.texCoords = 0;
	result.norm = 0;

	result.isInterlaced = isVertexDataInterlaced;
	result.posType = positionType;
	result.colorType = colorType;
	result.texCoordType = texCoordType;
	result.hasNormals = hasNormals;

	Assert(positionType >= 2 && (colorType >= 3 || colorType == 0) && (texCoordType == 0 || texCoordType == 2));

	if (isVertexDataInterlaced)
	{
		result.vertices = renderState->frameMemory.pushStructArray<real32>(vertexCount*(positionType+colorType+texCoordType+(hasNormals?3:0)));
	}
	else
	{
		result.pos = renderState->frameMemory.pushStructArray<real32>(vertexCount*positionType);
		result.colors = colorType ? renderState->frameMemory.pushStructArray<real32>(vertexCount*colorType) : NULL;
		result.texCoords = texCoordType ? renderState->frameMemory.pushStructArray<real32>(vertexCount*texCoordType) : NULL;
		result.norm = hasNormals ? renderState->frameMemory.pushStructArray<real32>(vertexCount*3) : NULL;
	}
	return result;
}

renderer_mesh_state gl_game_renderer::allocStaticMesh(
	memory_manager *memoryManager,
	int32 vertexCount,
	int32 image,
	bool32 isVertexDataInterlaced,
	renderer_position_type positionType,
	renderer_color_type colorType,
	renderer_tex_coord_type texCoordType,
	bool32 hasNormals)
{
	renderer_mesh_state result;
	result.totalVertexCount = vertexCount;
	result.usedVertexCount = 0;
	result.image = image;
	result.color.set(1.0f, 1.0f, 1.0f, 1.0f);

	result.pos = 0;
	result.colors = 0;
	result.texCoords = 0;
	result.norm = 0;

	result.isInterlaced = isVertexDataInterlaced;
	result.posType = positionType;
	result.colorType = colorType;
	result.texCoordType = texCoordType;
	result.hasNormals = hasNormals;

	Assert(positionType >= 2 && (colorType >= 3 || colorType == 0) && (texCoordType == 0 || texCoordType == 2));

	if (isVertexDataInterlaced)
	{
		result.vertices = memoryManager->pushStructArray<real32>(vertexCount*(positionType+colorType+texCoordType+(hasNormals?3:0)));
	}
	else
	{
		result.pos = memoryManager->pushStructArray<real32>(vertexCount*positionType);
		result.colors = colorType ? memoryManager->pushStructArray<real32>(vertexCount*colorType) : NULL;
		result.texCoords = texCoordType ? memoryManager->pushStructArray<real32>(vertexCount*texCoordType) : NULL;
		result.norm = hasNormals ? memoryManager->pushStructArray<real32>(vertexCount*3) : NULL;
	}
	return result;
}

void gl_game_renderer::drawMesh(renderer_mesh_state meshState)
{
	gl_render_command* meshCommand = createCommand(gl_render_command::TypeMesh);
	meshCommand->mesh.textureId = renderState->imageInfo[meshState.image].glTexture;
	meshCommand->mesh.mode = GL_TRIANGLES;
	meshCommand->mesh.vertCount = meshState.usedVertexCount;
	meshCommand->mesh.color = meshState.color;
	meshCommand->mesh.normals = meshState.norm;

	if (meshState.isInterlaced)
	{
		meshCommand->mesh.vertexDataInfo = VERTEX_DATA_IS_INTERLACED_FLAG;
		meshCommand->mesh.vertices = meshState.vertices;
	}
	else
	{
		meshCommand->mesh.vertexDataInfo = 0;
		meshCommand->mesh.vertices = meshState.pos;
		meshCommand->mesh.colors = meshState.colors;
		meshCommand->mesh.texCoords = meshState.texCoords;
		meshCommand->mesh.normals = meshState.norm;
	}

	switch (meshState.posType)
	{
		case RendererPosition2D:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_POSITION_TYPE_2D;
		break;
		case RendererPosition3D:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_POSITION_TYPE_3D;
		break;
	}

	switch (meshState.colorType)
	{
		case RendererColorRGB:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_COLOR_TYPE_RGB;
		break;
		case RendererColorRGBA:
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_COLOR_TYPE_RGBA;
		break;
	}

	if (meshState.texCoordType)
	{
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_HAS_TEX_COORDS;
	}

	if (meshState.hasNormals)
	{
		meshCommand->mesh.vertexDataInfo |= VERTEX_DATA_HAS_NORMALS;
	}
}

static image_state loadImage(image_resource image)
{
	image_state result = { };
	ILuint imageILHandle = ilGenImage();
	ilBindImage(imageILHandle);
	ILboolean success = ilLoadImage(imageFileNames[image]);
	if (success)
	{
		ILinfo imageInfo;
		iluGetImageInfo(&imageInfo);
		if (imageInfo.Origin == IL_ORIGIN_LOWER_LEFT)
		{
			iluFlipImage();
		}

		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if (success)
		{
			//				Assert(imageInfo.Bpp==32);
			GLuint imageGLHandle;
			glGenTextures(1, &imageGLHandle);
			glBindTexture(GL_TEXTURE_2D, imageGLHandle);
			//				glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,imageInfo.Width,imageInfo.Height,0,GL_RGBA,GL_UNSIGNED_BYTE,imageInfo.Data);
			gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imageInfo.Width, imageInfo.Height, GL_RGBA, GL_UNSIGNED_BYTE, imageInfo.Data);
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0);
			result.image = image;
			result.info.width = imageInfo.Width;
			result.info.height = imageInfo.Height;
			result.glTexture = imageGLHandle;
			result.fileTime = platformGetFileModifiedTime(imageFileNames[image]);
		}
	}
	ilBindImage(0);
	ilDeleteImage(imageILHandle);
	return result;
}

image_handle gl_game_renderer::getImage(image_resource image)
{
	image_handle result = 0;
	if (image > 0 && image < Image::Count)
	{
		if (renderState->imageInfo[image].glTexture == 0)
		{
			image_state info = loadImage(image);
			if (info.glTexture)
			{
				renderState->imageInfo[image] = info;
			}
			result = info.image;
		}
		else
		{
			result = renderState->imageInfo[image].image;
		}
	}
	return result;
}

image_info gl_game_renderer::getImageInfo(image_handle image)
{
	image_info result = { };
	if (renderState->imageInfo[image].image != 0 && image > 0 && image < Image::Count)
	{
		if (renderState->imageInfo[image].glTexture != 0)
		{
			result = renderState->imageInfo[image].info;
		}
	}
	return result;
}

void gl_game_renderer::checkIfResourcesChanged()
{
	for (int imageIndex = 1; imageIndex < Image::Count; imageIndex++)
	{
		if (renderState->imageInfo[imageIndex].glTexture != 0) //if it is loaded
		{
			uint64 curWriteTime = platformGetFileModifiedTime(imageFileNames[imageIndex]);
			if (curWriteTime != renderState->imageInfo[imageIndex].fileTime)
			{
				image_state info = loadImage(imageIndex);
				if (info.glTexture)
				{
					glDeleteTextures(1, &renderState->imageInfo[imageIndex].glTexture);
					renderState->imageInfo[imageIndex] = info;
				}
			}
		}
	}
}

