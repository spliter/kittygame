/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * platform_common.h
 *
 *  Created on: 3 kwi 2015
 *      Author: Miko Kuta
 */

#ifndef PLATFORM_COMMON_H_
#define PLATFORM_COMMON_H_
#include "../shared/types.h"
#include <limits>
#define INFINITY std::numeric_limits<float>::infinity()

uint64 platformGetFileModifiedTime(const char* fileName);//returns 0 on failure
#endif
