rem @echo off
call vcvars32.bat
set CommonCompilerFlags=-MTd -nologo -Gm- -GR- -EHsc- -Od -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4127 -wd4065 -FC -Z7
set DevILLibraries= ..\libs\bin\IL\DevIL.lib ..\libs\bin\IL\ILU.lib ..\libs\bin\IL\ILUT.lib
set Win32IncludeFlags= /I..\libs\include\
set Win32LinkerFlags= -incremental:no -opt:ref user32.lib gdi32.lib winmm.lib opengl32.lib GLU32.lib Shlwapi.lib XInput.lib %DevILLibraries%

IF NOT EXIST build mkdir build
pushd build

REM 32-bit build
 del *.pdb > NUL 2> NUL
cl %CommonCompilerFlags% ..\src\game\game.cpp -Fmgame.map -LD /link -incremental:no -opt:ref -PDB:game_%random%.pdb -EXPORT:gameUpdateAndRender
cl %CommonCompilerFlags%  /Fewin32_app.exe ..\src\main.cpp  %win32IncludeFlags% /link %Win32LinkerFlags%


popd

